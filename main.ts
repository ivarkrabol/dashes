import {
  MarkdownPostProcessor,
  MarkdownPostProcessorContext,
  MarkdownPreviewRenderer,
  Plugin
} from 'obsidian';

export default class DashesPlugin extends Plugin {

  public postprocessor: MarkdownPostProcessor = (
    el: HTMLElement,
    ctx: MarkdownPostProcessorContext
  ) => {
    let n, walk = document.createTreeWalker(el, NodeFilter.SHOW_TEXT, null, false);
    while (n = walk.nextNode()) {
      n.nodeValue = n.nodeValue.replace(/(?<!-)--(?!-)/g, '\u2013');
      n.nodeValue = n.nodeValue.replace(/(?<!-)---(?!-)/g, '\u2014');
    }
  }

  async onload() {
    console.log('loading plugin');
    MarkdownPreviewRenderer.registerPostProcessor(this.postprocessor);
  }

  onunload() {
    console.log('unloading plugin');
    MarkdownPreviewRenderer.unregisterPostProcessor(this.postprocessor);
  }
}
